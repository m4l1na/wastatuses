# импорт библиотек
from os import getcwd, remove
import re
from subprocess import getstatusoutput
from time import time, sleep
from blessed import Terminal
from platform import platform
import shutil

# создание объекта терминала
t = Terminal()

# назначение констант
CURRENT_PATH = getcwd()  # константа директории, в которой был запущен скрипт
WA_PATH = "sdcard/Android/media/com.whatsapp"  # константа с местоположением WhatsApp на устройстве
WA_STATUSES_PATH = f"{WA_PATH}/WhatsApp/Media/.Statuses"  # константа с местоположением статусов Whatsapp на устройстве
RE_DATATYPE = re.compile(
    r".+(?P<datatype>\..+)$"
)  # константа регулярного выражения для поиска неизвестных типов данных
RE_ADB_MESSAGES = re.compile(
    r"\*.+"
)  # константа регулярного выражения для поиска сообщений от ADB и последующего их удаления
PLATFORM = platform()  # константа с названием системы


# функция запуска различных команд ADB, с последующим возвращением результата
def adb_run(cmd: str):
    if "linux" in PLATFORM.lower():
        return getstatusoutput(f"adb {cmd}")[1]
    elif "windows" in PLATFORM.lower():
        return getstatusoutput(f"./adb.exe {cmd}")[1]
    else:
        print(f"{t.home}{t.clear_eos}{t.red_reverse}Неизвестная система.{t.normal}")
        waitForEnter()
        exit()


# функция ожидания нажатия "Enter"
def waitForEnter():
    print(t.move_down(5) + t.clear_eol + 'Нажмите "Enter", чтобы продолжить.')
    while True:
        with t.cbreak():
            if t.inkey().code == 343:
                break


# функция проверки и установки ADB
def checkForAdb():
    print(f"{t.home}{t.clear_eos}{t.blink_yellow_reverse}Проверка ADB...{t.normal}")
    result = adb_run("--version").split("\n")[0]
    if "Android Debug Bridge version" in result:
        print(f"{t.home}{t.clear_eos}{t.green_reverse}ADB найден.{t.normal}")
    else:
        if "linux" in PLATFORM.lower():
            print(
                f"{t.home}{t.clear_eos}{t.red_reverse}ADB не найден, либо установлен некорректно.{t.normal}"
            )
            print('Для установки ADB, введите в терминал "sudo apt-get install adb"')
            waitForEnter()
            exit()
        elif "windows" in PLATFORM.lower():
            print(
                f"{t.home}{t.clear_eos}{t.red_reverse}ADB не найден, либо установлен некорректно.{t.normal}"
            )
            yn = input("Установить ADB?(д/н) ").strip().lower()
            if yn == "д" or yn == "y":
                print(
                    f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Установка ADB...{t.normal}"
                )
                print(f"{t.move_down(1)}Импорт библиотек...")
                from requests import get
                from zipfile import ZipFile

                print(f"{t.move_up(1)}{t.clear_eol}Библиотеки импортированы.")
                print("Скачивание ADB...")
                data = get(
                    "https://dl.google.com/android/repository/platform-tools-latest-windows.zip"
                ).content
                with open("adb.zip", "wb") as file:
                    file.write(data)
                print(f"{t.move_up(1)}{t.clear_eol}ADB скачан.")
                print(f"{t.move_up(1)}{t.clear_eol}Распаковка...")
                with ZipFile("adb.zip", "r") as adb_zip:
                    adb_zip.extractall()
                remove("adb.zip")
                print(f"{t.move_up(1)}{t.clear_eol}ADB установлен.")
                print("Настройка ADB...")
                shutil.move("platform-tools/adb.exe", "./adb.exe")
                shutil.rmtree("platform-tools")
                print(f"{t.move_up(1)}{t.clear_eol()}ADB настроен.")
            else:
                print(f"{t.home}{t.clear_eos}{t.red_reverse}Прервано.{t.normal}")
                waitForEnter()
                exit()
        else:
            print(f"{t.home}{t.clear_eos}{t.red_reverse}Неизвестная система.{t.normal}")
            waitForEnter()
            exit()


# функция проверки подключения устройств
def findDevices():
    while True:
        print(
            f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Поиск устройств...{t.normal}"
        )
        devicesList = (
            RE_ADB_MESSAGES.sub(
                "", adb_run("devices").replace("List of devices attached\n", "")
            )
            .strip()
            .split("\n")
        )
        devicesNumber = 0 if devicesList[0] == "" else len(devicesList)
        if devicesNumber == 0:
            print(
                f"{t.home}{t.clear_eos}{t.red_reverse}Устройства не найдены.{t.normal}"
            )
            sleep(1)
        elif devicesNumber >= 2:
            print(
                f"{t.home}{t.clear_eos}{t.orange_reverse}Слишком много устройств, отключите лишние.{t.normal}"
            )
            sleep(1)
        else:
            print(
                f"{t.home}{t.clear_eos}{t.green_reverse}Устройство найдено.{t.normal}"
            )
            break


# функция поиска WhatsApp на устройстве
def findWhatsapp():
    print(f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Поиск WhatsApp...{t.normal}")
    if (
        adb_run(f'shell ls "{WA_PATH}"').strip()
        == f"ls: {WA_PATH}: No such file or directory"
    ):
        print(f"{t.home}{t.clear_eos}{t.red_reverse}WhatsApp не найден.{t.normal}")
        exit()
    print(f"{t.home}{t.clear_eos}{t.green_reverse}WhatsApp найден.{t.normal}")


# функция выбора действия
def chooseAction():
    while True:
        print(f"{t.home}{t.clear_eos}1) Загрузить статусы")
        print(f"2) Удалить статусы")
        print(f"3) Вывести статусы")
        print(f"4) Выйти")
        key = input(f"{t.move_down(1)}{t.clear_eos}Выберите действие: ")
        if key.strip() in ("1", "2", "3", "4"):
            return key
        else:
            print(f"{t.home}{t.clear_eos}{t.red_reverse}Неверное действие.{t.normal}")
            sleep(1)


# функция загрузки статусов с устройства
def downloadStatuses():
    print(f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Чтение статусов...{t.normal}")
    filesList = (
        adb_run(f'shell ls -A1 "{WA_STATUSES_PATH}/" | grep -v ".nomedia"')
        .strip()
        .split("\n")
    )
    filesNumber = len(filesList) if filesList[0] != "" else 0
    if filesNumber >= 1:
        yn = input(f"{t.home}{t.clear_eos}Загрузить статусы?(д/н) ").strip().lower()
        if yn == "д" or yn == "y":
            print(
                f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Загрузка статусов...{t.normal}"
            )
            TIME = time()
            adb_run(f'pull "{WA_STATUSES_PATH}/" "{CURRENT_PATH}/Statuses_{TIME}"')
            remove(f"{CURRENT_PATH}/Statuses_{TIME}/.nomedia")
            print(
                f'{t.home}{t.clear_eos}{t.green_reverse}Статусы сохранены в "{CURRENT_PATH}/Statuses_{TIME}".{t.normal}'
            )
        else:
            print(f"{t.home}{t.clear_eos}{t.red_reverse}Прервано.{t.normal}")
    else:
        print(f"{t.home}{t.clear_eos}{t.red_reverse}Статусы не найдены.{t.normal}")


# функция удаления статусов с устройства
def removeStatuses():
    print(f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Чтение статусов...{t.normal}")
    filesList = (
        adb_run(f'shell ls -A1 "{WA_STATUSES_PATH}/" | grep -v ".nomedia"')
        .strip()
        .split("\n")
    )
    filesNumber = len(filesList) if filesList[0] != "" else 0
    if filesNumber >= 1:
        yn = input(f"{t.home}{t.clear_eos}Удалить статусы?(д/н) ").strip().lower()
        if yn == "д" or yn == "y":
            print(
                t.home
                + t.clear_eos
                + t.red_reverse
                + "Эта опция навсегда удалит все статусы с вашего телефона без возможности восстановления."
            )
            print(
                "WhatsApp НЕ восстанавливает статусы удаленные таким образом."
                + t.normal
            )
            yn = input(
                f"{t.clear_eos}Вы уверены, что действительно хотите удалить все статусы?(д/н) "
            ).strip()
            if yn == "д" or yn == "y":
                print(
                    f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Удаление статусов...{t.normal}"
                )
                adb_run(f'shell rm -r "{WA_STATUSES_PATH}/!(.nomedia)"')
                print(
                    f"{t.home}{t.clear_eos}{t.green_reverse}Статусы удалены.{t.normal}"
                )
            else:
                print(f"{t.home}{t.clear_eos}{t.red_reverse}Прервано.{t.normal}")
        else:
            print(f"{t.home}{t.clear_eos}{t.red_reverse}Прервано.{t.normal}")
    else:
        print(f"{t.home}{t.clear_eos}{t.red_reverse}Статусы не найдены.{t.normal}")


# функция выведения статусов с устройства
def enumStatuses():
    print(f"{t.home}{t.clear_eos}{t.yellow_blink_reverse}Чтение статусов...{t.normal}")
    filesList = (
        adb_run(f'shell ls -A1 "{WA_STATUSES_PATH}/" | grep -v ".nomedia"')
        .strip()
        .split("\n")
    )
    filesNumber = len(filesList) if filesList[0] != "" else 0
    imagesNumber = 0
    videosNumber = 0
    otherFilesNumber = 0
    otherFilesDatatypes = []
    if filesNumber != 0:
        print(
            f"{t.home}{t.clear_eos}{t.green_reverse}Всего статусов - {filesNumber}:{t.normal}"
        )
        for fileName in filesList:
            if ".jpg" in fileName:
                imagesNumber += 1
            elif ".mp4" in fileName:
                videosNumber += 1
            else:
                otherFilesNumber += 1
                otherFilesDatatypes.append(
                    f'"{RE_DATATYPE.search(fileName).group("datatype")}"'
                )
        if imagesNumber >= 1:
            print(f"Изображения - {imagesNumber}.")
        if videosNumber >= 1:
            print(f"Видео - {videosNumber}.")
        if otherFilesNumber >= 1:
            print(f'Другое - {otherFilesNumber}({", ".join(otherFilesDatatypes)}).')
    else:
        print(f"{t.home}{t.clear_eos}{t.red_reverse}Статусы не найдены.{t.normal}")


# главная функция, руководящая всеми вышеперечисленными
def main():
    with t.fullscreen():
        checkForAdb()
        findDevices()
        findWhatsapp()
        while True:
            action = chooseAction()
            if action == "1":
                downloadStatuses()
            elif action == "2":
                removeStatuses()
            elif action == "3":
                enumStatuses()
            elif action == "4":
                yn = input(
                    f"{t.home}{t.clear_eos}Вы уверены, что хотите выйти?(д/н) "
                ).strip()
                if yn == "д" or yn == "y":
                    break
                else:
                    print(f"{t.home}{t.clear_eos}{t.red_reverse}Прервано.{t.normal}")
            waitForEnter()


# запуск главной функции
if __name__ == "__main__":
    main()
